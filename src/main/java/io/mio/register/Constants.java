package io.mio.register;

/**
 * Constants
 *
 * @author lry
 */
public class Constants {

    public static final String CATEGORY_KEY = "category";

    public static final String PROVIDERS_CATEGORY = "providers";

    public static final String CONSUMERS_CATEGORY = "consumers";

    public static final String ROUTERS_CATEGORY = "routers";

    public static final String CONFIGURATORS_CATEGORY = "configurators";

    public static final String DEFAULT_CATEGORY = PROVIDERS_CATEGORY;

    public static final String ENABLED_KEY = "enabled";

    public static final String DYNAMIC_KEY = "dynamic";

    public static final String REMOVE_VALUE_PREFIX = "-";

    public static final String BACKUP_KEY = "backup";

    public static final String PROTOCOL_KEY = "protocol";

    public static final String CHECK_KEY = "check";

    public static final String GROUP_KEY = "group";

    public static final String PATH_KEY = "path";

    public static final String INTERFACE_KEY = "interface";

    public static final String FILE_KEY = "file";

    public static final String CLASSIFIER_KEY = "classifier";

    public static final String VERSION_KEY = "version";

    public static final String ANY_VALUE = "*";

    public static final String EMPTY_PROTOCOL = "empty";

    public static final String ADMIN_PROTOCOL = "admin";

    public static final String PROVIDER_PROTOCOL = "provider";

    public static final String CONSUMER_PROTOCOL = "consumer";

    public static final String REGISTRY_FILE_SAVE_SYNC_KEY = "save.file";

    public static final String REGISTRY_RETRY_PERIOD_KEY = "retry.period";

    public static final int DEFAULT_REGISTRY_RETRY_PERIOD = 5 * 1000;

}
